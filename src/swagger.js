const Project = require("./Project");
const swaggerJSDoc = require("swagger-jsdoc");

/**
 * Swagger Docs definition/setup.
 */
const definition = {
  info: {
    title: "Boil API",
    description: "API For Boil JS",
    version: Project.version,
    license: Project.license
  },
  servers: [
    `http://localhost:${process.env.PORT}`,
    "http://boiljs.com",
    "https://boil.heroku.com"
  ]
};

/**
 * Swagger Docs options.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const Options = {
  definition,
  /** The dir to search for route docs. */
  apis: ["./src/routes/*.js"]
};

const Spec = swaggerJSDoc(Options);

module.exports = { Spec, UI: require("swagger-ui-express") };
