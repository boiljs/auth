const { connect } = require("mongoose");

/**
 * Initialize MongoDB connection.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const db_connect = async () => {
  try {
    const { connection } = await connect(process.env.MONGODB_URI || process.argv[2], {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true
    });

    console.log(`MONGO: Connected ${connection.host}`);
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};

module.exports = db_connect;
