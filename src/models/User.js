const joi = require("@hapi/joi");
const mongoose = require("mongoose");
const { convert } = require("joigoose")(mongoose);
const nanoid = require("nanoid");

/**
 * User Schema containing username, passwordHash, email, refreshToken,
 * createdAt and updatedAt.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const UserSchema = joi.object({
  uid: joi
    .string()
    .default(() => nanoid())
    .meta({ _mongoose: { unique: true } }),
  username: joi
    .string()
    .regex(
      new RegExp(
        `^[a-z0-9][a-z0-9_\-]{${process.env.MODEL_NAME_MIN - 1},${process.env
          .MODEL_NAME_MAX - 1}}$`,
        "i"
      )
    )
    .required()
    .meta({ _mongoose: { unique: true } }),
  email: joi
    .string()
    .email()
    .max(255)
    .meta({ _mongoose: { unique: true } })
    .required(),
  passwordHash: joi
    .string()
    .regex(/^\$\d+\w+\$\d+\$[0-9a-z!"#\$%\&'\()*\+\,\-\.\/:;<=>?@\\\^_]+$/i)
    .required(),
  createdAt: joi.date().default(Date.now),
  updatedAt: joi.date().default(Date.now)
});

/**
 * User Mongoose Schema converted from JOI schema.
 *
 * @since 0.1.0
 * @author Jonathan Augsutine
 */
const mongooseSchema = new mongoose.Schema(convert(UserSchema));

/**
 * Pre-save & pre-update mongoose hook which updates the document
 * `updatedAt` field.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
mongooseSchema.pre(/(save|update)/i, async function() {
  this.updatedAt = Date.now();
});

/**
 * User Mongoose Model.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const mongooseModel = mongoose.model("User", mongooseSchema);

module.exports = { model: mongooseModel, schema: UserSchema };
