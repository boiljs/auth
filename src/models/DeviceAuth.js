const joi = require("@hapi/joi");
const mongoose = require("mongoose");
const { convert } = require("joigoose")(mongoose);

/**
 * DeviceAuth Schema containing refresh token for a user's device.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const DeviceAuthSchema = joi.object({
  uid: joi.string().required(),
  deviceID: joi.string().max(36),
  refreshToken: joi
    .string()
    .default("")
    .meta({ _mongoose: { unique: true } }),
  updatedAt: joi.date().default(Date.now)
});

/**
 * User Mongoose Schema converted from JOI schema.
 *
 * @since 0.1.0
 * @author Jonathan Augsutine
 */
const mongooseSchema = new mongoose.Schema(convert(DeviceAuthSchema));

/**
 * Pre-save & pre-update mongoose hook which updates the document
 * `updatedAt` field.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
mongooseSchema.pre(/(save|update)/i, async function() {
  this.username_deviceID = this.username + "_" + this.deviceID;
  this.updatedAt = Date.now();
});

/**
 * User Mongoose Model.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const mongooseModel = mongoose.model("DeviceAuth", mongooseSchema);

module.exports = { model: mongooseModel, schema: DeviceAuthSchema };
