require("dotenv").config();

const db_connect = require("./db");
const makeServer = require("./server");

const main = async () => {
  // Connect to DB
  await db_connect();

  const server = await makeServer();

  server.listen(process.env.PORT, () => {
    console.log(`Listening on port ${process.env.PORT}`);
  });
};

main();
