const path = require("path");
const Project = require("./Project");
const express = require("express");
const cors = require("cors");
const routes = require("./routes");
const swagger = require("./swagger");

/**
 * Creates a new Express Server instance.
 *
 * @returns  A new Express Server with routing setup.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const create = async () => {
  const server = express();

  // parse bodies
  server.use(express.json());
  
  // Use CORS
  server.use(cors());

  // Use Static for JSDocs
  server.use("/jsodcs", express.static(path.join(__dirname, "../", "public", "jsdocs")));

  // Use Static for swagger UI docs
  server.use(/^\/(api-?)?docs$/i, swagger.UI.serve, swagger.UI.setup(swagger.Spec));

  // Setup Routes
  const base = "/api/" + Project.apiVersion;

  server.use(base, routes.register);
  server.use(base, routes.login);
  server.use(base, routes.refresh);

  return server;
};

module.exports = create;
