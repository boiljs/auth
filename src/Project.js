/**
 * Project Definition & info.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const Project = {
  version: "0.1.0",
  license: "GPL-3.0",
  apiVersion: "v1"
};

module.exports = Project;
