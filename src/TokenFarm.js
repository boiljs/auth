const jwt = require("jsonwebtoken");
const { DeviceAuth } = require("./models");

/**
 * Creates a new auth JWT.
 *
 * @param {string} username
 * @param {string} email
 * @returns {string} New JWT
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const authToken = (username, uid, email) => {
  return jwt.sign({ username, uid, email }, process.env.JWT_SECRET, {
    algorithm: process.env.JWT_ALGO,
    expiresIn: process.env.JWT_EXPIRE,
    issuer: process.env.JWT_ISSUER,
    audience: process.env.JWT_AUDIENCE,
    subject: username
  });
};

/**
 * Update or set DeviceAuth reference.
 *
 * @param {string} token - Refresh token.
 * @param {string} uid - User ID.
 * @param {string} deviceID - User's device ID.
 * @returns {Promise<boolean>} `true` if the DeviceAuth was set.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const updateDeviceAuth = async (token, uid, deviceID) => {
  // Build
  const deviceAuth = { uid, deviceID, refreshToken: token };

  // Validate
  let validatedDA;
  try {
    validatedDA = await DeviceAuth.schema.validateAsync(deviceAuth);
  } catch (_ignored) {
    return false;
  }

  // Check if DA exists
  let currentDA;
  try {
    currentDA = DeviceAuth.model.findOne({ uid, deviceID });
  } catch (_ignored) {
    return false;
  }

  // If exists, update
  if (currentDA) {
    try {
      await currentDA.updateOne(validatedDA);
    } catch (_ignored) {
      return false;
    }
  }
  // If NOT exists, create
  else {
    try {
      await DeviceAuth.model.create(validatedDA);
    } catch (_ignored) {
      return false;
    }
  }

  return true;
};

/**
 * Creates a new refresh JWT and updates DeviceAuth reference.
 *
 * @param {string} username - unique username
 * @param {string} email - user unique email
 * @param {string} uid - User ID.
 * @param {string} deviceID - User's device ID.
 * @returns {Promise<string>} New Refresh JWT or `null` if an error occured.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const refreshToken = async (username, email, uid, deviceID) => {
  const token = jwt.sign(
    { username, uid, email, deviceID },
    process.env.JWT_REFRESH_SECRET,
    {
      issuer: process.env.JWT_ISSUER,
      audience: process.env.JWT_AUDIENCE
    }
  );

  const updated = await updateDeviceAuth(token, uid, deviceID);

  return updated ? token : null;
};

/**
 * Validates a refresh JWT.
 *
 * @param {string} token Refresh token to validate.
 * @returns {object|false} The decoded token or `false` if invalid.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const validateTokenSignature = token => {
  try {
    verfied = jwt.verify(token, process.env.JWT_REFRESH_SECRET, {
      issuer: process.env.JWT_ISSUER,
      audience: process.env.JWT_AUDIENCE
    });
  } catch (_ignored) {
    return false;
  }
};

/**
 * Checks a refresh token against the valid token in the DB.
 *
 * @param {string} token - Refresh token to validate.
 * @param {string} uid - User ID.
 * @param {string} deviceID - User's device ID.
 * @returns {boolean} - `true` if the token is verified.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const verifyTokenValidity = async (token, uid, deviceID) => {
  let deviceAuth;
  try {
    deviceAuth = await DeviceAuth.model.findOne({ uid, deviceID });
  } catch (error) {
    return false;
  }

  return deviceAuth.refreshToken === token;
};

/**
 * Attempts to validate the given refresh token.
 *
 * @param {string} token - Refresh Token to validate.
 * @param {string} uid - User ID.
 * @param {string} deviceID - Device ID.
 * @returns {Promise<boolean>} `true` if the token is validated.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const validate = async (token, uid, deviceID) => {
  // Validate locally
  const validated = validateTokenSignature(token);

  // Verify DB token
  const verified = verifyTokenValidity(token, uid, deviceID);

  return verified ? validated : false;
};

/**
 * Attempts to decode the given JWT.
 *
 * @param {string} token JWT to validate.
 * @returns {object|undefined} The decoded JWT data or undefined if an error occurs.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const decode = token => {
  try {
    return jwt.decode(token, { complete: true });
  } catch (error) {
    return undefined;
  }
};

module.exports = { authToken, refreshToken, validate, decode };
