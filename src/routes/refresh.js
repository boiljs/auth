const express = require("express");
const router = express.Router();
const packetier = require("packetier");
const TokenFarm = require("../TokenFarm");

/**
 * POST /api/v#/token-refresh
 *
 * @swagger
 * /api/v1/token-refresh:
 *  post:
 *    description: Use refresh token to get new auth token
 *    parameters:
 *      - name: refresh_token
 *        description: The refresh token
 *        type: string
 *        in: body
 *        required: true
 *      - name: deviceID
 *        description: The client device ID
 *        type: string
 *        in: body
 *        required: true
 *    responses:
 *      200:
 *        description: Refresh token valid & new auth-token returned.
 *      401:
 *        description: Refresh token missing or invalid.
 *      500:
 *        description: An error occurred validating or refreshing the token.
 */
router.post(/^\/(token\-)?refresh$/i, async (req, res) => {
  const refresh_token = req.body["refresh-token"] || req.body.refresh_token;

  if (!refresh_token) {
    return res
      .status(401)
      .json(packetier(false, null, { err: "Missing refresh token" }));
  }

  let valid = await TokenFarm.validate(refresh_token, req.body.deviceID);

  if (!valid) {
    return res
      .status(401)
      .json(packetier(false, null, { err: "Invalid refresh token" }));
  }

  // Decode token
  const decoded = TokenFarm.decode(refresh_token);

  /**  @type {{ username: string, email: string, uid: string }} */
  const payload = decoded.payload;

  // new auth token
  let token;
  try {
    token = TokenFarm.authToken(payload.username, payload.uid, payload.email);
  } catch (error) {
    return res
      .status(500)
      .json(packetier(false, null, { err: "Internal: token" }));
  }

  res.status(200).json(packetier(true, { token }));
});

module.exports = router;
