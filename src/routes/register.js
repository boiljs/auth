const express = require("express");
const router = express.Router();
const packetier = require("packetier");
const bcrypt = require("bcryptjs");
const TokenFarm = require("../TokenFarm");
const { User, DeviceAuth } = require("../models");
const nanoid = require("nanoid");

/**
 * Hashes the given password.
 *
 * @param {string} password Password to hash.
 * @returns {Promise<string>} The hashed password
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const enhash = async password => {
  return await bcrypt.hash(password, parseInt(process.env.BCRYPT_SALT_ROUNDS));
};

/**
 * Handles user registration/account creation.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 *
 * @swagger
 *
 * /api/v1/auth/signup:
 *  post:
 *    description: Registers a new user
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: user
 *        description: username and password
 *        in: body
 *        required: true
 *        type: string
 *        schema:
 *          $ref: '#/definitions/NewUser'
 *    responses:
 *      200:
 *        description: user created
 *        content:
 *          application/json:
 *      400:
 *        description: missing properties
 *      409:
 *        description: username taken
 */
router.post(/^\/(signup|register)$/i, async (req, res) => {
  // Pull properties
  const { username, password, email } = req.body;

  // Ensure username, password, and email exist
  if (!(username && password && email)) {
    if (!username) {
      return res
        .status(400)
        .json(packetier(false, null, { err: "Missing username" }));
    } else if (!password) {
      return res
        .status(400)
        .json(packetier(false, null, { err: "Missing password" }));
    } else if (!email) {
      return res
        .status(400)
        .json(packetier(false, null, { err: "Missing email" }));
    }
  }

  // Ensure password is valid length
  if (password.length > process.env.AUTH_PASS_MAX) {
    return res.status(400).json(
      packetier(false, null, {
        err: `Password too long (max ${process.env.AUTH_PASS_MAX} char)`
      })
    );
  } else if (password.length < process.env.AUTH_PASS_MIN) {
    return res.status(400).json(
      packetier(false, null, {
        err: `Password too short (min ${process.env.AUTH_PASS_MIN} char)`
      })
    );
  }

  // Hash Password
  let hashed = "";
  try {
    hashed = await enhash(password);
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json(packetier(false, null, { err: "Internal err: hashing" }));
  }

  // Validate
  let user;
  try {
    user = await User.schema.validateAsync({
      username,
      email,
      passwordHash: hashed
    });
  } catch (error) {
    return res.status(400).json(packetier(false, null, { err: error.message }));
  }

  // Save
  /** Saved user MongoDB Document */
  let userDoc;
  try {
    userDoc = await User.model.create(user);
  } catch (error) {
    if (error.code) {
      switch (error.code) {
        case 11000:
          return res
            .status(409)
            .json(packetier(false, null, { err: "username taken" }));
      }
    }
    return res.status(500).end();
  }

  // Generate a new device ID
  let deviceID = nanoid(parseInt(process.env.DEVICE_ID_LENGTH));

  // Generate new token
  let authToken;
  try {
    authToken = TokenFarm.authToken(username, userDoc.uid, email);
  } catch (error) {
    return response
      .status(500)
      .json(packetier(false, null, { err: "Internal: token" }));
  }

  // Attempt to generate refresh token
  let refreshToken;
  try {
    refreshToken = await TokenFarm.refreshToken(
      userDoc.username,
      userDoc.email,
      userDoc.uid,
      deviceID
    );
  } catch (error) {
    return response
      .status(500)
      .json(packetier(false, null, { err: "Internal: refresh token" }));
  }

  const cleanUser = userDoc.toObject();
  delete cleanUser.__v;
  delete cleanUser._id;
  delete cleanUser.passwordHash;

  // Return new user
  return res
    .status(201)
    .header("auth-token", authToken)
    .cookie("auth-token", authToken)
    .header("refresh-token", refreshToken)
    .cookie("refresh-token", refreshToken)
    .json(
      packetier(true, cleanUser, { token: authToken, refreshToken, deviceID })
    );
});

module.exports = router;
