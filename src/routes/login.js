const express = require("express");
const router = express.Router();
const packetier = require("packetier");
const bcrypt = require("bcryptjs");
const TokenFarm = require("../TokenFarm");
const { User, DeviceAuth } = require("../models");
const nanoid = require("nanoid");

/**
 * Checks the given password against the given hash.
 *
 * @param {string} password The password to check.
 * @param {string} hash The hash to check against.
 * @returns {Promise<boolean>} `true` if the password was validated.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const checkHash = async (password, hash) => {
  return await bcrypt.compare(password, hash);
};

/**
 * @since 0.1.0
 * @author Jonathan Augustine
 *
 * TODO swagger docs
 * @swagger
 * /api/v1/login:
 *  post:
 *    description: User login
 *    parameters:
 *      - name: username
 *      - name: email
 *      - name: uid
 *      - name: password
 *      - name: deviceID
 *
 */
router.post("/login", async (req, res) => {
  // get password
  const { username, email, password, uid } = req.body;
  const deviceID = req.body.deviceID || nanoid(process.env.DEVICE_ID_LENGTH);

  // Validate deviceID
  if (
    typeof deviceID !== "string" &&
    deviceID.length !== process.env.DEVICE_ID_LENGTH
  ) {
    return res
      .status(400)
      .json(packetier(false, null, { err: "Invalid device ID" }));
  }

  // Confirm password and username or email exist
  if (!password) {
    return res
      .status(401)
      .json(packetier(false, null, { err: "Missing password" }));
  } else if (!(username || email || uid)) {
    return res
      .status(401)
      .json(packetier(false, null, { err: "Missing username or email" }));
  }

  // Build query
  const query = {};
  if (email) query.email = email;
  else if (username) query.username = username;
  else query.uid = uid;

  // Find user
  /** @type {{username: string, uid: string, email: string, passwordHash: string}} */
  let userDoc;
  try {
    userDoc = await User.model.findOne(query);
  } catch (error) {
    console.error(error);
    return res
      .status(404)
      .json(packetier(false, null, { err: "User not found" }));
  }

  if (!userDoc) {
    return res
      .status(404)
      .json(packetier(false, null, { err: "User not found" }));
  }

  // Validate password
  let validated;
  try {
    validated = await checkHash(password, userDoc.passwordHash);
  } catch (error) {
    return res
      .status(500)
      .json(packetier(false, null, { err: "Internal: token" }));
  }

  if (!validated) {
    return res
      .status(401)
      .json(packetier(false, null, { err: "Wrong password" }));
  }

  // Update refresh token
  let refreshToken;
  try {
    refreshToken = await TokenFarm.refreshToken(
      userDoc.username,
      userDoc.email,
      userDoc.uid,
      deviceID
    );

    if (!refreshToken) throw new Error();
  } catch (error) {
    res
      .status(500)
      .json(packetier(false, null, { err: "Internal: refresh token" }));
  }

  // Make new auth token
  let authToken;
  try {
    authToken = TokenFarm.authToken(
      userDoc.username,
      userDoc.uid,
      userDoc.email
    );
  } catch (error) {
    res
      .status(500)
      .json(packetier(false, null, { err: "Internal: auth token" }));
  }

  const cleanUser = userDoc.toObject();
  delete cleanUser.__v;
  delete cleanUser._id;
  delete cleanUser.passwordHash;

  // Return user
  return res
    .status(201)
    .header("auth-token", authToken)
    .cookie("auth-token", authToken)
    .header("refresh-token", refreshToken)
    .cookie("refresh-token", refreshToken)
    .json(packetier(true, cleanUser, { authToken, refreshToken, deviceID }));
});

module.exports = router;
