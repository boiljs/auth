/**
 * @swagger
 *
 * definitions:
 *  NewUser:
 *    type: object
 *    required:
 *      required:
 *        - username
 *        - password
 *      properties:
 *        username:
 *          type: string
 *        email:
 *          type: string
 *        password:
 *          type: string
 *          format: password
 *
 */

module.exports = {
  login: require("./login"),
  register: require("./register"),
  refresh: require("./refresh")
};
