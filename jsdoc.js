module.exports = {
  source: {
    include: ["./src"],
    includePattern: ".js$",
    excludePattern: "(node_modules/|docs)"
  },
  plugins: ["plugins/markdown"],
  templates: {
    cleverLinks: true,
    monospaceLinks: true
  },
  opts: {
    template: "jsdoc_template",
    recurse: true,
    destination: `./public/jsdocs/`
  }
};
